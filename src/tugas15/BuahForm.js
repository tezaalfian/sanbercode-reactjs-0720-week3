import React, { useContext, useState, useEffect } from "react";
import { BuahContext } from "./BuahContext";
import axios from "axios";

const BuahForm = () => {
  const [input, setInput] = useState({ nama: "", harga: 0, berat: 0, id: "" });
  const [daftarBuah, setDaftarBuah, idBuah] = useContext(BuahContext);

  const handleChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (input.nama.replace(/\s/g, "") !== "") {
      if (input.id === "") {
        axios
          .post(`http://backendexample.sanbercloud.com/api/fruits`, {
            name: input.nama,
            price: input.harga,
            weight: input.berat,
          })
          .then((res) => {
            setDaftarBuah([
              ...daftarBuah,
              {
                nama: res.data.name,
                harga: res.data.price,
                berat: res.data.weight,
                id: res.data.id,
              },
            ]);
          });
      } else {
        axios
          .put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {
            name: input.nama,
            price: input.harga,
            weight: input.berat,
          })
          .then((res) => {
            let buah = daftarBuah.filter((el) => el.id === res.data.id);
            buah = {
              nama: res.data.name,
              harga: res.data.price,
              berat: res.data.weight,
              id: res.data.id,
            };
            const buahLama = daftarBuah.filter((el) => el.id != res.data.id);
            setDaftarBuah([...buahLama, buah]);
          });
      }
    }
    setInput({ nama: "", harga: 0, berat: 0, id: "" });
  };

  useEffect(() => {
    if (idBuah !== "") {
      const buah = daftarBuah.filter((el) => el.id == idBuah);
      setInput(...buah);
    }
  }, [idBuah]);

  return (
    <div className="list-tabel">
      <h1 align="center">Form Harga Buah</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Masukkan Nama Buah:</label>
          <input
            type="text"
            value={input.nama}
            onChange={handleChange}
            name="nama"
          />
        </div>
        <div>
          <label>Masukkan Harga Buah:</label>
          <input
            type="number"
            value={input.harga}
            onChange={handleChange}
            name="harga"
          />
        </div>
        <div>
          <label>Masukkan Berat Buah:</label>
          <input
            type="number"
            value={input.berat}
            onChange={handleChange}
            name="berat"
          />
        </div>
        <button>Submit</button>
      </form>
    </div>
  );
};

export default BuahForm;
