import React from "react";
import Tugas11 from "../tugas11/Tugas11";
import Timer from "../tugas12/Timer";
import Lists from "../tugas13/Lists";
import List from "../tugas14/List";
import Buah from "../tugas15/Buah";
import Home from "../tugas15/Home";
import { Switch, Route } from "react-router";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/tugas11">
        <Tugas11 />
      </Route>
      <Route exact path="/timer">
        <Timer start="100" />
      </Route>
      <Route exact path="/lists">
        <Lists />
      </Route>
      <Route exact path="/list">
        <List />
      </Route>
      <Route exact path="/buah">
        <Buah />
      </Route>
    </Switch>
  );
};

export default Routes;
