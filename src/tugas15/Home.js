import React from "react";
import "../tugas15/list.css";

const Home = () => {
  return (
    <>
      <div class="home">
        <h1>Home</h1>
        <p>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Veniam
          dolorum, sit qui perferendis dicta quas eveniet. Corrupti voluptates
          magni repudiandae earum nam eius, fugit odio, praesentium alias eos
          odit nisi.
        </p>
      </div>
    </>
  );
};

export default Home;
