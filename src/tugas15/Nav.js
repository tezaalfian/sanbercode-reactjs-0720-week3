import React, { useState } from "react";
import "../tugas15/list.css";
import { Link } from "react-router-dom";
const Nav = () => {
  const [colorTheme, setColorTime] = useState({
    background: "#4b5d67",
    color: "#fff",
  });

  const themeChange = () => {
    setColorTime({
      ...colorTheme,
      background: colorTheme.color,
      color: colorTheme.background,
    });
  };

  return (
    <>
      <nav style={{ backgroundColor: colorTheme.background }}>
        <p style={{ color: colorTheme.color }} onClick={themeChange}>
          Color Theme
        </p>
        <ul>
          <li>
            <Link to="/" style={{ color: colorTheme.color }}>
              Home
            </Link>
          </li>
          <li>
            <Link to="/tugas11" style={{ color: colorTheme.color }}>
              Tugas 11
            </Link>
          </li>
          <li>
            <Link to="/timer" style={{ color: colorTheme.color }}>
              Tugas 12
            </Link>
          </li>
          <li>
            <Link to="/lists" style={{ color: colorTheme.color }}>
              Tugas 13
            </Link>
          </li>
          <li>
            <Link to="/list" style={{ color: colorTheme.color }}>
              Tugas 14
            </Link>
          </li>
          <li>
            <Link to="/buah" style={{ color: colorTheme.color }}>
              Tugas 15
            </Link>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default Nav;
