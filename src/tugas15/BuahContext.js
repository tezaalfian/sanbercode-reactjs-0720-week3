import React, { useState, createContext } from "react";

export const BuahContext = createContext();

export const BuahProvider = (props) => {
  const [daftarBuah, setDaftarBuah] = useState(null);
  const [idBuah, setIdBuah] = useState("");

  return (
    <BuahContext.Provider
      value={[daftarBuah, setDaftarBuah, idBuah, setIdBuah]}
    >
      {props.children}
    </BuahContext.Provider>
  );
};
