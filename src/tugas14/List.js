import React, { useState, useEffect } from "react";
import axios from "axios";
import "../tugas14/list.css";

const List = () => {
  const [daftarBuah, setdaftarBuah] = useState(null);
  const [input, setInput] = useState({ nama: "", harga: 0, berat: 0, id: "" });
  const [statusForm, setStatusForm] = useState("create");

  const handleChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (input.nama.replace(/\s/g, "") !== "") {
      if (statusForm === "create") {
        axios
          .post(`http://backendexample.sanbercloud.com/api/fruits`, {
            name: input.nama,
            price: input.harga,
            weight: input.berat,
          })
          .then((res) => {
            setdaftarBuah([
              ...daftarBuah,
              {
                nama: res.data.name,
                harga: res.data.price,
                berat: res.data.weight,
                id: res.data.id,
              },
            ]);
          });
      } else if (statusForm === "edit") {
        axios
          .put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {
            name: input.nama,
            price: input.harga,
            weight: input.berat,
          })
          .then((res) => {
            const buah = daftarBuah.filter((el) => el.id === res.data.id);
            setdaftarBuah([
              ...buah,
              {
                nama: res.data.name,
                harga: res.data.price,
                berat: res.data.weight,
                id: res.data.id,
              },
            ]);
            console.log(res.data);
          });
      }
      setStatusForm("create");
    }

    setInput({ nama: "", harga: 0, berat: 0, id: "" });
  };

  const deleteBuah = (e) => {
    let id = parseInt(e.target.value);
    setdaftarBuah(daftarBuah.filter((el) => el.id !== id));
    axios
      .delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
      .then((res) => {
        console.log(res);
      });
  };

  const editBuah = (e) => {
    axios
      .put(`http://backendexample.sanbercloud.com/api/fruits/${e.target.value}`)
      .then((res) => {
        const buah = {
          nama: res.data.name,
          harga: res.data.price,
          berat: res.data.weight,
          id: res.data.id,
        };
        setInput({ ...buah });
      });
    setStatusForm("edit");
  };

  useEffect(() => {
    if (daftarBuah === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
          const buah = res.data
            .map((el) => {
              return {
                nama: el.name,
                harga: el.price,
                berat: el.weight,
                id: el.id,
              };
            })
            .filter((el) => el.nama !== null);
          setdaftarBuah(buah);
        });
    }
  }, [daftarBuah]);

  return (
    <div className="list-tabel">
      <h1 align="center">Tabel Harga Buah</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {daftarBuah !== null &&
            daftarBuah.map((el, i) => {
              return (
                <tr key={el.id}>
                  <td>{++i}</td>
                  <td>{el.nama}</td>
                  <td>{el.harga}</td>
                  <td>{el.berat / 1000} Kg</td>
                  <td>
                    <button value={el.id} onClick={editBuah}>
                      Edit
                    </button>{" "}
                    <button value={el.id} onClick={deleteBuah}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>

      {/* Form */}
      <h1 align="center">Form Harga Buah</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Masukkan Nama Buah:</label>
          <input
            type="text"
            value={input.nama}
            onChange={handleChange}
            name="nama"
          />
        </div>
        <div>
          <label>Masukkan Harga Buah:</label>
          <input
            type="number"
            value={input.harga}
            onChange={handleChange}
            name="harga"
          />
        </div>
        <div>
          <label>Masukkan Berat Buah:</label>
          <input
            type="number"
            value={input.berat}
            onChange={handleChange}
            name="berat"
          />
        </div>
        <button>Submit</button>
      </form>
    </div>
  );
};

export default List;
