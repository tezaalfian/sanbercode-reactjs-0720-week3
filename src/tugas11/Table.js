import React from "react";

class Table extends React.Component {
  render() {
    return (
      <table style={{ border: "1px solid #000", margin: "0 auto", width: "100%" }}>
        <thead style={{ backgroundColor: "#b3aeae" }}>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
          </tr>
        </thead>
        <tbody style={{ background: "#fa8072" }}>
          {this.props.data.map((el) => {
            return (
              <tr>
                <td>{el.nama}</td>
                <td>{el.harga}</td>
                <td>{el.berat / 1000} Kg</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default Table;
