import React from "react";
import Header from "./Header";
import Table from "./Table";

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]
class Tugas11 extends React.Component {
  render() {
    return (
      <>
      <Header title="Tabel Harga Buah" />
      <Table data={dataHargaBuah}/>
      </>
    )
  }
}

export default Tugas11;
