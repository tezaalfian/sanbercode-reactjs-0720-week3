import React from "react";
// import Tugas11 from "./tugas11/Tugas11";
// import Timer from "./tugas12/Timer";
// import Lists from "./tugas13/Lists";
// import Lists from "./tugas14/ListsBuah";
// import Buah from "./tugas15/Buah";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./tugas15/Routes";
import "./App.css";
import Nav from "./tugas15/Nav";

function App() {
  return (
    <>
      {/* Tugas 11 */}
      {/* <Tugas11 /> */}

      {/* Tugas 12 */}
      {/* <Timer start="100"/> */}

      {/* Tugas 13 */}
      {/* <Lists /> */}

      {/* Tugas 14 */}
      {/* <Lists /> */}

      {/* Tugas 15 */}
      <Router>
        <Nav />
        <Routes />
      </Router>
    </>
  );
}

export default App;
